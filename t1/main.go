package main

import (
	"database/sql"
	"fmt"
	"log"
	"math/rand"
	"sync"
	"time"

	_ "github.com/go-sql-driver/mysql"
)

const (
	dsn         = "root:root@tcp(127.0.0.1:3306)/tang"
	numInsert   = 10000
	numUpdate   = 10000
	numWorkers  = 10
	insertQuery = `INSERT INTO test_table (
                        LAST_QTY, LAST_PRICE, TRANSACTION_TS, ORDER_ID,
                        CLIENT_ORDER_ID, LIMIT_PRICE, INSTR_ID, TRADER_ID,
                        ACCT, APP_ID, DIRECTION, MATCH_ID, FLAGS
                    ) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)`
	updateQuery = `UPDATE test_table SET LAST_PRICE = ? WHERE TRANSACTION_ID = ?`
)

func main() {
	db, err := sql.Open("mysql", dsn)
	if err != nil {
		log.Fatalf("Failed to connect to database: %v", err)
	}
	defer db.Close()

	// Create table
	createTable(db)

	// Insert performance test
	start := time.Now()
	performInserts(db)
	duration := time.Since(start)
	fmt.Printf("INSERT: %d operations in %v, QPS: %f\n", numInsert, duration, float64(numInsert)/duration.Seconds())

	// Update performance test
	start = time.Now()
	performUpdates(db)
	duration = time.Since(start)
	fmt.Printf("UPDATE: %d operations in %v, QPS: %f\n", numUpdate, duration, float64(numUpdate)/duration.Seconds())
}

func createTable(db *sql.DB) {
	_, err := db.Exec(`
        CREATE TABLE IF NOT EXISTS test_table (
            TRANSACTION_ID BIGINT PRIMARY KEY AUTO_INCREMENT,
            LAST_QTY INT,
            LAST_PRICE DECIMAL(10, 2),
            TRANSACTION_TS TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
            ORDER_ID BIGINT,
            CLIENT_ORDER_ID BIGINT,
            LIMIT_PRICE DECIMAL(10, 2),
            INSTR_ID INT,
            TRADER_ID INT,
            ACCT VARCHAR(20),
            APP_ID INT,
            DIRECTION VARCHAR(10),
            MATCH_ID BIGINT,
            FLAGS INT
        )
    `)
	if err != nil {
		log.Fatalf("Failed to create table: %v", err)
	}
}

func performInserts(db *sql.DB) {
	var wg sync.WaitGroup
	tasks := make(chan int, numInsert)

	for i := 0; i < numWorkers; i++ {
		wg.Add(1)
		go func() {
			defer wg.Done()
			for range tasks {
				_, err := db.Exec(insertQuery,
					rand.Intn(1000),    // LAST_QTY
					rand.Float64()*100, // LAST_PRICE
					time.Now(),         // TRANSACTION_TS
					rand.Int63(),       // ORDER_ID
					rand.Int63(),       // CLIENT_ORDER_ID
					rand.Float64()*100, // LIMIT_PRICE
					rand.Intn(1000),    // INSTR_ID
					rand.Intn(1000),    // TRADER_ID
					"ACCT"+fmt.Sprintf("%d", rand.Intn(1000)), // ACCT
					rand.Intn(1000), // APP_ID
					"BUY",           // DIRECTION
					rand.Int63(),    // MATCH_ID
					rand.Intn(100),  // FLAGS
				)
				if err != nil {
					log.Printf("Failed to insert: %v", err)
				}
			}
		}()
	}

	for i := 0; i < numInsert; i++ {
		tasks <- i
	}
	close(tasks)
	wg.Wait()
}

func performUpdates(db *sql.DB) {
	var wg sync.WaitGroup
	tasks := make(chan int, numUpdate)

	for i := 0; i < numWorkers; i++ {
		wg.Add(1)
		go func() {
			defer wg.Done()
			for task := range tasks {
				_, err := db.Exec(updateQuery, rand.Float64()*100, task+1) // 假设 TRANSACTION_ID 是从 1 开始递增
				if err != nil {
					log.Printf("Failed to update: %v", err)
				}
			}
		}()
	}

	for i := 0; i < numUpdate; i++ {
		tasks <- i
	}
	close(tasks)
	wg.Wait()
}
