CREATE TABLE test_table (
    TRANSACTION_ID BIGINT PRIMARY KEY AUTO_INCREMENT,
    LAST_QTY INT,
    LAST_PRICE DECIMAL(10, 2),
    TRANSACTION_TS TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    ORDER_ID BIGINT,
    CLIENT_ORDER_ID BIGINT,
    LIMIT_PRICE DECIMAL(10, 2),
    INSTR_ID INT,
    TRADER_ID INT,
    ACCT VARCHAR(20),
    APP_ID INT,
    DIRECTION VARCHAR(10),
    MATCH_ID BIGINT,
    FLAGS INT
);
