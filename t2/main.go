package main

import (
	"database/sql"
	"fmt"
	"log"
	"math/rand"
	"sync"
	"time"

	_ "github.com/lib/pq"
)

const (
	dsn         = "postgres://user:password@127.0.0.1:5432/demodb?sslmode=disable"
	numInsert   = 10000
	numUpdate   = 10000
	numWorkers  = 10
	insertQuery = `INSERT INTO test_table (
                        last_qty, last_price, transaction_ts, order_id,
                        client_order_id, limit_price, instr_id, trader_id,
                        acct, app_id, direction, match_id, flags
                    ) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13)`
	updateQuery = `UPDATE test_table SET last_price = $1 WHERE transaction_id = $2`
)

func main() {
	db, err := sql.Open("postgres", dsn)
	if err != nil {
		log.Fatalf("Failed to connect to database: %v", err)
	}
	defer db.Close()

	// Create table
	createTable(db)

	// Insert performance test
	start := time.Now()
	performInserts(db)
	duration := time.Since(start)
	fmt.Printf("INSERT: %d operations in %v, QPS: %f\n", numInsert, duration, float64(numInsert)/duration.Seconds())

	// Update performance test
	start = time.Now()
	performUpdates(db)
	duration = time.Since(start)
	fmt.Printf("UPDATE: %d operations in %v, QPS: %f\n", numUpdate, duration, float64(numUpdate)/duration.Seconds())
}

func createTable(db *sql.DB) {
	_, err := db.Exec(`
        CREATE TABLE IF NOT EXISTS test_table (
            transaction_id SERIAL PRIMARY KEY,
            last_qty INT,
            last_price NUMERIC(10, 2),
            transaction_ts TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
            order_id BIGINT,
            client_order_id BIGINT,
            limit_price NUMERIC(10, 2),
            instr_id INT,
            trader_id INT,
            acct VARCHAR(20),
            app_id INT,
            direction VARCHAR(10),
            match_id BIGINT,
            flags INT
        )
    `)
	if err != nil {
		log.Fatalf("Failed to create table: %v", err)
	}
}

func performInserts(db *sql.DB) {
	var wg sync.WaitGroup
	tasks := make(chan int, numInsert)

	for i := 0; i < numWorkers; i++ {
		wg.Add(1)
		go func() {
			defer wg.Done()
			for range tasks {
				_, err := db.Exec(insertQuery,
					rand.Intn(1000),    // LAST_QTY
					rand.Float64()*100, // LAST_PRICE
					time.Now(),         // TRANSACTION_TS
					rand.Int63(),       // ORDER_ID
					rand.Int63(),       // CLIENT_ORDER_ID
					rand.Float64()*100, // LIMIT_PRICE
					rand.Intn(1000),    // INSTR_ID
					rand.Intn(1000),    // TRADER_ID
					"ACCT"+fmt.Sprintf("%d", rand.Intn(1000)), // ACCT
					rand.Intn(1000), // APP_ID
					"BUY",           // DIRECTION
					rand.Int63(),    // MATCH_ID
					rand.Intn(100),  // FLAGS
				)
				if err != nil {
					log.Printf("Failed to insert: %v", err)
				}
			}
		}()
	}

	for i := 0; i < numInsert; i++ {
		tasks <- i
	}
	close(tasks)
	wg.Wait()
}

func performUpdates(db *sql.DB) {
	var wg sync.WaitGroup
	tasks := make(chan int, numUpdate)

	for i := 0; i < numWorkers; i++ {
		wg.Add(1)
		go func() {
			defer wg.Done()
			for task := range tasks {
				_, err := db.Exec(updateQuery, rand.Float64()*100, task+1) // 假设 TRANSACTION_ID 是从 1 开始递增
				if err != nil {
					log.Printf("Failed to update: %v", err)
				}
			}
		}()
	}

	for i := 0; i < numUpdate; i++ {
		tasks <- i
	}
	close(tasks)
	wg.Wait()
}
